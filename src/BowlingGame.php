<?php

namespace AbdulSamad\Bowling;

class BowlingGame {
    /**
     * The number of players in a game.
     *
     * @var integer
     */
    const PLAYERS_PER_GAME = 1;
    /**
     * Players Names
     *
     * @var array
     */
    protected $players = [];
    /**
     * Score of the players
     *
     * @var array
     */
    protected $score = [];
    /**
     * The CLI object
     *
     * @var object
     */
    protected $console;
    /**
     * The number of frames in a game.
     *
     * @var integer
     */
    const FRAMES_PER_GAME = 10;
    /**
     * All registered rolls/throws for a game.
     *
     * @var array
     */
    protected $rolls = [];

    /**
     * Constructor to set the players on class initialization
     *
     * @var object
     */
    function __construct($console)
    {
        $this->console = $console;
        //Take the input for all the players available in the game
        for($player = 0; $player < self::PLAYERS_PER_GAME ; $player++){
            if(self::PLAYERS_PER_GAME == 1){
                $playerName = $this->console->getInput('Enter Your Name:');
            }else{
                $playerName = $this->console->getInput('Enter Player '. ($player + 1) .' Name:');
            }
            if(empty(trim($playerName))){
                $this->players[] = 'Player '. ($player + 1);
            }else{
                $this->players[] = trim($playerName);
            }
            $player++;
        }
    }

    /**
     * Record the rolls/throws of the players
     *
     * @return void
     */
    public function roll()
    {
        $frame = 0;
        while($frame < self::FRAMES_PER_GAME) {
            foreach ($this->players as $id => $player) {
                $this->console->clear();
                $this->console->print(strtoupper($player) . ' is playing now...');
                $turn = 0;
                while ($turn < 2) {
                    $roll = (int) $this->console->getInput('Enter roll:');
                    $this->rolls[$player][$frame][] = $roll;
                    if(count($this->rolls[$player][$frame]) < 3 && $roll == 10){

                    }else{
                        $turn++;
                    }
                }
            }
            $frame++;
        }
    }

    /**
     * Calculate the final score of the game.
     *
     * @return array
     */
    public function score()
    {
        $frame = 0;
        while($frame < self::FRAMES_PER_GAME) {
            foreach($this->players as $player) {
                $currentFrame = $this->rolls[$player][$frame];
                //Run the loop against the throws in a frame
                if($this->isStrike($currentFrame)){
                    //Check if Strike & add the bonus accordingly
                    $score[$player][] = $this->strikeBonus($currentFrame);
                }elseif($this->isSpare($this->rolls[$player][$frame])){
                    //Check if isSpare & add the bonus accordingly
                    $score[$player][] = $this->spareBonus($currentFrame);
                }else{
                    $score[$player][] = $currentFrame[0] + $currentFrame[1];
                }
                //Record the score of the frame
                if($frame > 0){
                    $score[$player][$frame] = $score[$player][$frame]+ $score[$player][$frame-1];
                }
            }
            $frame++;
        }
        return $this->score = $score;
    }
    /**
     * To calcuate the score and display on the board
     * @return void
     */
    public function displayScore()
    {
        $frameScores = $this->score();// To calculate score
        foreach($frameScores as $player => $score){
            $this->console->print(strtoupper($player) .' : [' . implode(',',$score) . ']');
        }
    }
    /**
     * Did the player make a spare?
     *
     * @param $frame
     * @return bool
     */
    private function isSpare($frame)
    {
        return (count($frame) == 2 && (($frame[0] + $frame[1]) == 10));
    }
    /**
     * Did the player make a strike?
     *
     * @param $frame
     * @return bool
     */
    private function isStrike($frame)
    {
        return (count($frame)!= 2 && $frame[0] == 10);
    }
    /**
     * Get the default sum of the two rolls for a frame.
     *
     * @param $roll
     * @return mixed
     */
    private function getDefaultFrameScore($roll)
    {
        return $this->rolls[$roll] + $this->rolls[$roll + 1];
    }
    /**
     * Get the bonus for a strike in a frame.
     *
     * @param $frame
     * @return mixed
     */
    private function strikeBonus($frame)
    {
        return (10 + $frame[1] + $frame[2]);
    }
    /**
     * Get the bonus for a spare in a frame.
     *
     * @param $frame
     * @return mixed
     */
    private function spareBonus($frame)
    {
        return (10 + $frame[1]);
    }
}