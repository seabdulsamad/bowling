<?php

namespace AbdulSamad\Bowling;

use AbdulSamad\Bowling\Entities\SymfonyCli;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AbdulSamad\Bowling\BowlingGame;

/**
 * Class App
 *
 * @package AbdulSamad\App
 */
class App extends command
{

    /**
     * Configures the command
     *
     * @author Abdul Samad <se.abdulsamad@gmail.com>
     */
    protected function configure()
    {
        $this->setName('bowling');
    }

    /**
     * DESC
     *
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @author Abdul Samad <se.abdulsamad@gmail.com>
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $console = new SymfonyCli();
        $console->clear();
        $console->heading('WELCOME TO BOWLING');
        $console->print('==============================================================', 'comment');
        $bowling = new BowlingGame($console);
        $console->print('==============================================================', 'comment');
        $console->print('Game Started');
        $bowling->roll();
        $console->print('==============================================================', 'comment');
        $console->clear();
        $console->heading('Game Over');
        $console->heading('Score Board');
        $console->print('==============================================================', 'comment');
        $bowling->displayScore();
    }

}