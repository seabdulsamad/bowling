<?php
namespace Tests;

use AbdulSamad\Bowling\BowlingGame;
use AbdulSamad\Bowling\Entities\SymfonyCli;

/**
 * Class Test
 *
 * @package Tests
 */
class GameTest extends TestCase
{
    /** @var Game */
    private $game;

    public function setUp()
    {
        $console = new SymfonyCli();
        $this->game = new BowlingGame($console);
    }
}