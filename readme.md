# Bowling Game

The project is a small but interesting bowling game for two players using 10 frames. The application is developed using core PHP (See the composer.php file more details about dependencies).

## Getting Started

Clone the project on your machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You need to install the followings on your machine in order to setup the development environment on your  machine.

```
PHP >=7.1
```
Install the [composer](https://getcomposer.org/) The dependency manager on your machine to install the project dependencies.
```
PHP composer
```
Install the git on your machine
```
git
```
### Installing

Clone the project using the command below.

```
$ git clone https://bitbucket.org/seabdulsamad/bowling-game.git
```

Install project dependencies by running this command in the project's root directory:

```
$ composer install
```
If you change structure, paths, namespaces, etc., make sure you run the [autoload generator](https://getcomposer.org/doc/03-cli.md#dump-autoload):

```sh
$ composer dump-autoload
```
To run the application via console use the above command.
```
$ php index.php
```
## Running the tests

To run the tests:
```
$ php vendor/phpunit/phpunit/phpunit
```

## Built With

* CORE PHP
* [Composer](https://getcomposer.org/doc/) - Dependency Management

## Assumptions

* The game will consist of 10 frames along with 2 players. You may change the frames from BowlingGame CLASS

## Authors

* **Abdul Samad** - se.abdulsamad@gmail.com

## License

This project is licensed under the MIT License [Abdul Samad](http://linkedin.com/in/abdul-samad-993b8450)