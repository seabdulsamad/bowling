#!/user/env/php
<?php

require_once __DIR__ . '/vendor/autoload.php';

use AbdulSamad\Bowling\App;
use Symfony\Component\Console\Application;

$bowling = new App();

$app = new Application();

$app->add($bowling);
$app->setDefaultCommand($bowling->getName());

$app->run();